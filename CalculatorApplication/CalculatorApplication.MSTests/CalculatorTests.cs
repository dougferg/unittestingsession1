﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorApplication.MSTests
{
    [TestClass]
    public class CalculatorTests
    {
        private Calculator _sut;

        [TestInitialize]
        public void Init()
        {
            _sut = new Calculator();    
        }

        [TestMethod]
        public void Add_two_plus_two_equals_four()
        {
            //Arrange
            const int expected = 4;

            //Act
            _sut.Add(2, 2);

            //Assert
            Assert.AreEqual(expected, _sut.Result);
        }
    }
}
