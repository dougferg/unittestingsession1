﻿using NUnit.Framework;

namespace CalculatorApplication.NUnitTests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator _sut;

        [SetUp]
        public void Init()
        {
            _sut = new Calculator();
        }

        [Test]
        public void Add_two_plus_two_equals_four()
        {
            //Arrange
            const int expected = 4;

            //Act
            _sut.Add(2, 2);

            //Assert
            Assert.AreEqual(expected, _sut.Result);

        }

        [Test]
        public void Add_three_plus_three_equals_six()
        {
            //Arrange
            const int expected = 6;

            //Act
            _sut.Add(3, 3);

            //Assert
            Assert.AreEqual(expected, _sut.Result);

        }

        ////Don't forget to test for edge cases.
        //[Test]
        //public void Add_test_what_happens_when_we_exceed_int_max()
        //{
        //    //Arrange
        //    const int expected = 6; //What will happen?  

        //    //Act
        //    _sut.Add(int.MaxValue, 3);

        //    //Assert
        //    Assert.AreEqual(expected, _sut.Result);
        //}

        [Test]
        public void two_plus_two_plus_one_equals_five()
        {
            //Arrange
            const int expected = 5;
            //Act
            _sut.Add(2, 2);
            _sut.Add(1);

            //Assert
            Assert.AreEqual(expected, _sut.Result);
        }

        [Test]
        public void One_plus_two_plus_two_equals_five()
        {
            //Arrange
            const int expected = 5;
            //Act

            _sut.Add(1);
            _sut.Add(2, 2);

            //Assert
            Assert.AreEqual(expected, _sut.Result);
        }
    }
}
