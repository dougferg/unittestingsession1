﻿namespace CalculatorApplication
{
    public class Calculator
    {
        public int Result { get; private set; }
        
        public void Add(int toAdd, int second)
        {
            Result += toAdd + second;
        }

        public void Add(int toAdd)
        {
            Result += toAdd;
        }
    }
}
